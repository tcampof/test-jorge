var Waterline = require('waterline');

var Provincia = Waterline.Collection.extend({
  identity: 'provincia',
  datastore: 'default',
  primaryKey: '_id',

  attributes: {
    _id: {
      type: 'string',
      autoMigrations: { autoIncrement: true },
    },
    nombre: {
      type: 'string',
    },
    // Relación con usuarios
    usuarios: {
      collection: 'usuario',
      via: 'id_provincia'
    },
    empresa: {
      collection: 'empresa',
      via: 'id_provincia'
    }
  }
});

module.exports = Provincia;