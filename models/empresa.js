var Waterline = require('waterline');

var Empresa = Waterline.Collection.extend({
  identity: 'empresa',
  datastore: 'default',
  primaryKey: '_id',

  attributes: {
    _id: {
      type: 'string',
      autoMigrations: { autoIncrement: true },
    },
    nombre: {
      type: 'string',
    },
    pais: {
      type: 'string',
      defaultsTo: 'ESPAÑA'
    },
    id_provincia: {
      type: 'string',
    },
    poblacion: {
      type: 'string',
    },
    direccion: {
      type: 'string',
    },
    cp: {
      type: 'string',
    },
    telefono: {
      type: 'string',
      allowNull: true
    },

    id_provincia: {
      model: 'provincia',
    },
    // Relación con usuarios
    usuarios: {
      collection: 'usuario',
      via: 'id_empresa'
    }
  }
});

module.exports = Empresa;
