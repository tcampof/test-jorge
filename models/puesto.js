var Waterline = require('waterline');

var Puesto = Waterline.Collection.extend({
  identity: 'puesto',
  datastore: 'default',
  primaryKey: '_id',

  attributes: {
    _id: {
      type: 'string',
      autoMigrations: { autoIncrement: true },
    },
    nombre: {
      type: 'string',
    },
    categoria: {
      type: 'string',
    },
    // Relación con usuarios
    usuarios: {
      collection: 'usuario',
      via: 'id_puesto'
    }
  }
});

module.exports = Puesto;
