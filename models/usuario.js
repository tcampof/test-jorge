var Waterline = require('waterline');

var Usuarios = Waterline.Collection.extend({
  identity: 'usuario',
  datastore: 'default',
  primaryKey: '_id',

  attributes: {
    _id: {
      type: 'string',
      autoMigrations: { autoIncrement: true },
    },
    nombre: {
      type: 'string',
    },
    email: {
      type: 'string',
    },
    apellidos: {
      type: 'string',
    },
    direccion: {
      type: 'string',
    },
    id_puesto: {
      type: 'string',
      columnName: 'id_puesto'
    },
    id_provincia: {
      type: 'string',
      columnName: 'id_provincia'
    },
    id_empresa: {
      type: 'string',
      columnName: 'id_empresa'
    },
    id_puesto: {
      model: 'puesto',
    },
    id_provincia: {
      model: 'provincia',
    },
    id_empresa: {
      model: 'empresa',
    },
  }
});

module.exports = Usuarios;