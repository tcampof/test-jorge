'use strict'
const http = require('http');
const https = require('https');
const fs = require('fs');
const express = require('express');
require('dotenv').config({path: './config.env'});
const { error } = require('console');
const datosBD = require('./database')

let puerto = process.env.PORT
const url_proyecto = process.env.URL_PROYECTO;
let tipo = process.env.DB_PROVIDER


let conectar = false;
let puertoBD = 0;
if(tipo){
    const app = require('./app');
    console.log("Arrancando entorno de desarrollo...");
    conectando();
    app.listen(puerto, function(){
        console.log("Servidor API Rest escuchando en http://"+url_proyecto+":" + puerto);
    });
    
}else{
    console.log("SIN BASE DE DATOS")
    conectar = false;
    puertoBD = 0;
}

// Pasamos a la conexion para que realice la conexion con la base de datos.
async function conectando(){
    try {
        await datosBD.conexion();
    } catch (error) {
        console.log(error);
    }
}
