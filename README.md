## Inicio de proyecto
### Instalacion de componentes:
Iniciamos el proyecto de node e instalamos: waterline y los adaptadores necesarios: sails-mongo, sails-mysql etc... mediante:
```
npm install waterline
npm install sails-mongo
npm install sails-mysql

```
## CREAMOS LA CARPETA config CON EL ARCHIVO conexiondb.js 
En dicho fichero agregaremos las configuraciones de las cadenas de conexion de las bases de datos con el codigo: 
```
var sailsMongoAdapter = require('sails-mongo');
var sailsMysqlAdapter = require('sails-mysql');
require('dotenv').config({path: './config.env'});
// Obtenemos el tipo de base de datos desde el fichero de configuracion.
let tipo = process.env.DB_PROVIDER

if(tipo.toUpperCase() == "MONGODB"){
  console.log("Conexion MongoDB");
  var config = {
    adapters: {
      mongodb: sailsMongoAdapter
    },
    datastores: {
      default: {
        adapter: 'mongodb',
        url: 'mongodb://localhost:27017/pruebaORM'
      }
    }
  };
}else if (tipo.toUpperCase() == "MYSQL"){
  console.log("Conexion MySQL");
  var config = {
    adapters: {
      mysql: sailsMysqlAdapter
    },
    datastores: {
      default: {
        adapter: 'mysql',
        url: 'mysql://root@localhost:3306/pruebaORM'
      }
    }
  };
}else{
  console.log("SIN CONFIGURACION DE BASE DE DATOS");
}

module.exports = {
    config
}

```
## CREACION BASE DE DATOS:
### MONGODB.
En mongo db solamente creamos la coleccion. Ya que al agregar campos se agregan automaticamente los documentos.
### SQL:
Tenemos que crear las tablas al inicio, ###IMPORTANTE: SE TIENE QUE PONER QUE EL ID SE CREE AUTOMATICAMENTE, AUTONUMERICO O UUID ###, adjunto el codigo del ejemplo:
```
CREATE DATABASE IF NOT EXISTS pruebaORM;
USE pruebaORM;

CREATE TABLE provincia(
    _id VARCHAR(75) PRIMARY KEY DEFAULT (UUID()),
    nombre VARCHAR(75) NOT NULL
);

CREATE TABLE puesto(
    _id VARCHAR(75) PRIMARY KEY DEFAULT (UUID()),
    nombre VARCHAR(75) NOT NULL,
    categoria VARCHAR(75)
);

CREATE TABLE empresa(
    _id VARCHAR(75) PRIMARY KEY DEFAULT (UUID()),
    nombre VARCHAR(75) NOT NULL,
    pais VARCHAR(75) DEFAULT 'España',
    id_provincia VARCHAR(75),
    poblacion VARCHAR(75),
    direccion VARCHAR(75),
    cp VARCHAR(5),
    telefono VARCHAR(10),
    FOREIGN KEY (id_provincia) REFERENCES provincia(_id)
);

CREATE TABLE usuario(
    _id VARCHAR(75) PRIMARY KEY DEFAULT (UUID()),
    nombre VARCHAR(75) NOT NULL,
    email VARCHAR(75) NOT NULL,
    apellidos VARCHAR(150),
    direccion VARCHAR(150),
    id_puesto VARCHAR(75),
    id_provincia VARCHAR(75),
    id_empresa VARCHAR(75),
    FOREIGN KEY (id_puesto) REFERENCES puesto(_id),
    FOREIGN KEY (id_provincia) REFERENCES provincia(_id),
    FOREIGN KEY (id_empresa) REFERENCES empresa(_id)
);
```
Sembrar base de datos, para que puedas insertar usuarios:
```
INSERT INTO provincia (_id, nombre)
VALUES ('64368e2ead5d5d509fb797e9', 'JAEN');

INSERT INTO puesto (_id, nombre, categoria)
VALUES ('64368e7dad5d5d509fb797eb', 'APRENDIZ', 'APRENDIZ');

INSERT INTO empresa (_id, nombre, pais, direccion, id_provincia, poblacion, telefono,cp)
VALUES ('64368e99ad5d5d509fb797ee', 'ACERCA ST', 'ESPAÑA', 'c/ cuesta Nº 2', '64368e2ead5d5d509fb797e9', 'La Carolina', '657657657','23200');
```

### MODELOS:
Adjuntamos ejemplo de los modelos Usuarios el cual tiene 3 claves foraneas: Empresa, Provincia, Puesto:
```
var Waterline = require('waterline');

var Usuarios = Waterline.Collection.extend({
  identity: 'usuario',
  datastore: 'default',
  primaryKey: '_id',

  attributes: {
    _id: {
      type: 'string',
      autoMigrations: { autoIncrement: true },
    },
    nombre: {
      type: 'string',
    },
    email: {
      type: 'string',
    },
    apellidos: {
      type: 'string',
    },
    direccion: {
      type: 'string',
    },
    id_puesto: {
      type: 'string',
      columnName: 'id_puesto'
    },
    id_provincia: {
      type: 'string',
      columnName: 'id_provincia'
    },
    id_empresa: {
      type: 'string',
      columnName: 'id_empresa'
    },
    id_puesto: {
      model: 'puesto',
    },
    id_provincia: {
      model: 'provincia',
    },
    id_empresa: {
      model: 'empresa',
    },
  }
});

module.exports = Usuarios;
```
Para que tengan relacion en las tablas que tienen la relacion tenemos que hacer referencia a la principal, por ejemplo adjuntamos tabla provincia:
```
var Waterline = require('waterline');

var Provincia = Waterline.Collection.extend({
  identity: 'provincia',
  datastore: 'default',
  primaryKey: '_id',

  attributes: {
    _id: {
      type: 'string',
      autoMigrations: { autoIncrement: true },
    },
    nombre: {
      type: 'string',
    },
    // Relación con usuarios
    usuarios: {
      collection: 'usuario',
      via: 'id_provincia'
    },
    empresa: {
      collection: 'empresa',
      via: 'id_provincia'
    }
  }
});

module.exports = Provincia;
```

### CONDICIONES: 
Condiciones: (Solamente aplica a la tabla principal, no a las unidas)
  Sin ninguna condicion: 
  ```
  let condiciones = {}
  ```
  Con una condicion: 
  ```
  let condiciones = {'nombre':'Paquito'}
  ```
  Con condiciones AND 
  ```
  let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}
  ```
  con condiciones OR  
  ```
  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } 
  ```
  Ejemplo 2:
  ```
  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }
  ```
  Expresiones similares: Ejemplo buscar nombres parecido a Juan: 
  ```
  let condiciones = { name: { 'like': '%Juan%' }}
  ```
  Negacion: 
  ```
  let condiciones = {'nombre': { '!=': 'Paquito' }}; 
  ```
## FICHEROS DE CONEXION Y CRUD
Creamos un fichero llamado database.js con la conexion, y los metodos de obtener la coleccion: Insertar, lectura, lectura individual, actualizacion, borrado. 
### CONEXION:
En la conexion devolvemos "true" o "false" si conesguimos la conexion:
```
async function conexion(){
  let = conexionRealizada = false;
  new Promise((resolve, reject) => {
    var waterline = new Waterline();

    // Cargamos los modelos en Waterline, uno por cada linea.
    waterline.registerModel(Usuario);
    waterline.registerModel(Empresa);
    waterline.registerModel(Puesto);
    waterline.registerModel(Provincia);

    // Cargamos la configuracion.
    var config = configbd.config

    waterline.initialize(config, function(err, ontology) {
        if (!err) {
            //Aqui agregamos cada uno de los modelos a cargar por cada modelo de esquema.
            empresaModelo = ontology.collections.empresa;
            provinciaModelo = ontology.collections.provincia;
            puestoModelo = ontology.collections.puesto;
            usuarioModelo = ontology.collections.usuario;

            conexionRealizada = true;
            resolve(); 
        }else{
            console.error(err)
        }
    })
  })
  return conexionRealizada;
}
```
Creamos la funcion que cada vez que entramos en una funcion del CRUD obtenemos el modelo correspondiente.
```
function leerModelo(tipo){
  if(tipo.toUpperCase() == "EMPRESA"){
    return empresaModelo;
  }else if(tipo.toUpperCase() == "PROVINCIA"){
    return provinciaModelo;
  }else if(tipo.toUpperCase() == "PUESTO"){
    return puestoModelo;
  }else if(tipo.toUpperCase() == "USUARIO"){
    return usuarioModelo;
  }
}
```
### INSERTAR: 
Se le pasa el modelo de datos y el tipo por ejemplo: "USUARIO"
```
// Funcion de insertar un componente:
function insertar(model, tipo){
    var modelo = leerModelo(tipo);
    
    return modelo.create(model)
      .then(function () {
        console.log(tipo,' creado con éxito:');
        return true;
      })
      .catch(function (error) {
        console.log(error)
        console.error('Hubo un error creando:', tipo);
        return false;
      });
};
```
### LISTAR LA TABLA:  
Tipo: Es un string el cual viene con el nombre de la tabla, ejemplo: "USUARIO"  
Condiciones: (Solamente aplica a la tabla principal)  
  * Sin ninguna condicion: let condiciones = {}  
  * Con una condicion: let condiciones = {'nombre':'Paquito'}  
  * Con condiciones AND let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}  
  * con condiciones OR  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } Ejemplo 2:  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }  
  * Expresiones similares: Ejemplo buscar nombres parecido a Juan: let condiciones = { name: { 'like': '%Juan%' }}  
  * Negacion: let condiciones = {'nombre': { '!=': 'Paquito' }};

id_tablas: Se trata de una Array con los id que relacionan con otras columnas, por ejemplo en la tabla usuario tienen los campos relacionados: id_empresa, id_provincia y id_puesto se pasaria de la siguiente manera: 
```
let id_tablas = ['id_empresa','id_provincia','id_puesto']; 
 ```
 La funcion de listar es: 
```
//Leer toda la tabla con una condicion
function leerTodos(tipo,condiciones, id_tablas) {
  var modelo = leerModelo(tipo);
  //Creamos la consulta en funcion de si tenemos condiciones o no 
  let query
  if (condiciones != {} && condiciones != null && condiciones != undefined){
    query = modelo.find(condiciones);
  }else{
    query = modelo.find();
  }
  // Relacionamos cada tabla por el id.
  id_tablas.forEach(function(id_tablas) {
    query = query.populate(id_tablas);
  });

  //devolvemos el resultado. EN caso de tener undefined devolvemos un array vacio.
  return query.then(function(resultados) {
    if (resultados == undefined) return [] // Si llega vacio, limpiamos resultados.
    return resultados;  
  })
  .catch(function(err) {
    console.error(err);
    return [];
  });
};
```
### LEER UN ELEMENTO
Para dicha función, aunque acepta varias condiciones igual que en la función anterior. Si se recomienda que en este caso se le pase en la condición el id. Ejemplo: 
```
let condicion = {'_id':id}
```
La funcion de obtener la lectura es:
```
function leerUno(tipo,condiciones, id_tablas){
  var modelo = leerModelo(tipo);

  if (condiciones){
    let query
    query = modelo.findOne(condiciones);
    //asignamos las relaciones de tablas.
    id_tablas.forEach(function(id_tablas) {
      query = query.populate(id_tablas);
    });
    //devolvemos el resultado. EN caso de tener undefined devolvemos un array vacio.
    return query.then(function(resultado) {
      if (resultado == undefined) return []; // Si llega vacio, limpiamos resultado.
      return [resultado];  
    })
    .catch(function(err) {
      console.error(err);
      return [];
    });
  }else{
    return [];
  }
};
```
### ACTUALIZAR 
Las variables que les llegan son:  
Tipo: Es un string el cual viene con el nombre de la tabla, ejemplo: "USUARIO"  
Condiciones: (Solamente aplica a la tabla principal)  

  * Sin ninguna condicion: let condiciones = {}, (Actualizaria todos)
  * Con una condicion: let condiciones = {'_id':id}; (Actualizaria el _id que coincida)  
  * Con condiciones AND let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}  
  * con condiciones OR  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } Ejemplo 2:  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }  
  * Expresiones similares: Ejemplo buscar nombres parecido a Juan: let condiciones = { name: { 'like': '%Juan%' }}  
  * Negacion: let condiciones = {'nombre': { '!=': 'Paquito' }};

modeloEsquema: Es el modelo con los campos actualizados. Puedes pasar el modelo completo o los campos a modificar.

La funcion de actualizar es:
```
// Actualizar con condiciones
function actualizar(tipo,condiciones, modeloEsquema) {
  var modelo = leerModelo(tipo);

  if(condiciones != null && condiciones != undefined && modeloEsquema != null && modeloEsquema != undefined){
    return modelo.update(condiciones,modeloEsquema)
      .then(function () {
        console.log(tipo,' actualizado con éxito:');
        return true;
      })
      .catch(function (error) {
        console.log(error)
        console.error('Hubo un error al actualizar:', tipo);
        return false;
      });
  }else{
    return false;
  }
}
```
### BORRAR
Las variables que les llegan son:  
Tipo: Es un string el cual viene con el nombre de la tabla, ejemplo: "USUARIO"  
Condiciones: Sigue la misma estructura que actualizacion.  
Ejemplo para eliminar un id:  
```
let condiciones = {'_id':id};
```
La funcion de borrar es:
```
// Borrar con condiciones para no eliminar toda la tabla.
function borrar(tipo,condiciones) {
  var modelo = leerModelo(tipo);

  if(condiciones != null && condiciones != undefined){
    return modelo.destroy(condiciones)
      .then(function () {
        console.log(tipo,' eliminado');
        return true;
      })
      .catch(function (error) {
        console.log(error)
        console.error('Hubo un error al eliminar:', tipo);
        return false;
      });
  }else{
    return false;
  }
} 
```
