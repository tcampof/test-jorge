'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

//El comentario de abajo no se puede borrar
//<cargar_rutas>
const RutasUsuarios = require('./routes/usuarios');
const RutasEmpresas = require('./routes/empresa');
const RutasProvincias = require('./routes/provincia');
const RutasPuestos = require('./routes/puesto');


//Configuración de body-parser 
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json()); //Aquí convertimos todo el HTML que nos llegue a la API a json

//configurar cabeceras http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


//El comentario de abajo no se puede borrar
//<usar_rutas>
app.use('/api',RutasUsuarios);
app.use('/api',RutasEmpresas);
app.use('/api',RutasProvincias);
app.use('/api',RutasPuestos);

module.exports = app;