//Importacion de libreria waterline:
var Waterline = require('waterline');
//Importacion de configuracion de la base de datos
const configbd = require('./config/conexionbd')
// Importacion de la base de datos:
var Usuario = require('./models/usuario');
var Empresa = require('./models/empresa');
var Puesto = require('./models/puesto');
var Provincia = require('./models/provincia');

// creamos los modelos de esquemas:
var usuarioModelo;
var empresaModelo;
var puestoModelo;
var provinciaModelo;

// Creamos la conexion y asignamos los 
async function conexion(){
  let = conexionRealizada = false;
  new Promise((resolve, reject) => {
    var waterline = new Waterline();

    // Cargamos los modelos en Waterline, uno por cada modelo.
    waterline.registerModel(Usuario);
    waterline.registerModel(Empresa);
    waterline.registerModel(Puesto);
    waterline.registerModel(Provincia);

    // Cargamos la configuracion.
    var config = configbd.config

    waterline.initialize(config, function(err, ontology) {
        if (!err) {
            //Aqui agregamos cada uno de los modelos a cargar por cada modelo de esquema.
            empresaModelo = ontology.collections.empresa;
            provinciaModelo = ontology.collections.provincia;
            puestoModelo = ontology.collections.puesto;
            usuarioModelo = ontology.collections.usuario;

            conexionRealizada = true;
            resolve(); 
        }else{
            console.error(err)
        }
    })
  })
  return conexionRealizada;
}

// Aqui leemos el modelo principal con el que trabajamos uno por cada esquema para realizar la petición:
function leerModelo(tipo){
  if(tipo.toUpperCase() == "EMPRESA"){
    return empresaModelo;
  }else if(tipo.toUpperCase() == "PROVINCIA"){
    return provinciaModelo;
  }else if(tipo.toUpperCase() == "PUESTO"){
    return puestoModelo;
  }else if(tipo.toUpperCase() == "USUARIO"){
    return usuarioModelo;
  }else{
    return null
  }
}

// Funcion de insertar un componente:
function insertar(model, tipo){
    var modelo = leerModelo(tipo);
    
    return modelo.create(model)
      .then(function () {
        console.log(tipo,' creado con éxito:');
        return true;
      })
      .catch(function (error) {
        console.log(error)
        console.error('Hubo un error creando:', tipo);
        return false;
      });
};

// Funcion para leer un componente, en este ejemplo un usuario. Tenemos que tener como minimo la condicion de leer un ID 
function leerUno(tipo,condiciones, id_tablas){
  var modelo = leerModelo(tipo);

  if (condiciones){
    let query
    query = modelo.findOne(condiciones);
    //asignamos las relaciones de tablas.
    id_tablas.forEach(function(id_tablas) {
      query = query.populate(id_tablas);
    });
    //devolvemos el resultado. EN caso de tener undefined devolvemos un array vacio.
    return query.then(function(resultado) {
      if (resultado == undefined) return []; // Si llega vacio, limpiamos resultado.
      return [resultado];  
    })
    .catch(function(err) {
      console.error(err);
      return [];
    });
  }else{
    return [];
  }
};

//Leer toda la tabla con una condicion
function leerTodos(tipo,condiciones, id_tablas) {
  var modelo = leerModelo(tipo);
  //Creamos la consulta en funcion de si tenemos condiciones o no 
  let query
  if (condiciones != {} && condiciones != null && condiciones != undefined){
    query = modelo.find(condiciones);
  }else{
    query = modelo.find();
  }
  // Relacionamos cada tabla por el id.
  id_tablas.forEach(function(id_tablas) {
    query = query.populate(id_tablas);
  });

  //devolvemos el resultado. EN caso de tener undefined devolvemos un array vacio.
  return query.then(function(resultados) {
    if (resultados == undefined) return [] // Si llega vacio, limpiamos resultados.
    return resultados;  
  })
  .catch(function(err) {
    console.error(err);
    return [];
  });
};

// Actualizar con condiciones
function actualizar(tipo,condiciones, modeloEsquema) {
  var modelo = leerModelo(tipo);

  if(condiciones != null && condiciones != undefined && modeloEsquema != null && modeloEsquema != undefined){
    return modelo.update(condiciones,modeloEsquema)
      .then(function () {
        console.log(tipo,' actualizado con éxito:');
        return true;
      })
      .catch(function (error) {
        console.log(error)
        console.error('Hubo un error al actualizar:', tipo);
        return false;
      });
  }else{
    return false;
  }
}
// Borrar con condiciones para no eliminar toda la tabla.
function borrar(tipo,condiciones) {
  var modelo = leerModelo(tipo);

  if(condiciones != null && condiciones != undefined){
    return modelo.destroy(condiciones)
      .then(function () {
        console.log(tipo,' eliminado');
        return true;
      })
      .catch(function (error) {
        console.log(error)
        console.error('Hubo un error al eliminar:', tipo);
        return false;
      });
  }else{
    return false;
  }
}


module.exports = {
  conexion,
  insertar,
  leerUno,
  leerTodos,
  actualizar,
  borrar
};