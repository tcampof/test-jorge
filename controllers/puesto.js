'use strict'
const fs = require('fs')
const bd = require('../database')

/* Condiciones: (Solamente aplica a la tabla principal)
  Sin ninguna condicion: let condiciones = {}
  Con una condicion: let condiciones = {'nombre':'Paquito'}
  Con condiciones AND let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}
  con condiciones OR  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } Ejemplo 2:  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }
  Expresiones siminlares: Ejemplo buscar nombres parecido a Juan: let condiciones = { name: { 'like': '%Juan%' }}
  Negacion: let condiciones = {'nombre': { '!=': 'Paquito' }}; 
 */
async function listarPuestos(req,res){
    let nombre_funcion = "puesto/listarPuestos"
    let codigo = 2;
    let listado = [];
    let condiciones = {}; 
    let id_tablas = [];
    try{
        listado = await bd.leerTodos("PUESTO",condiciones,id_tablas)
        if(listado.length > 0){
        codigo = 1;
        }else{
        codigo = 3
        }
    }finally { 
        res.status(200).send({error_code: codigo, response:{listado}, message: nombre_funcion});
    }
}

module.exports = {
    listarPuestos,
};