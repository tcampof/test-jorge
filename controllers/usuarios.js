'use strict'
const fs = require('fs')
const bd = require('../database')

async function crearUsuario(req,res){
    var params = req.body;
    let nombre_funcion="Usuarios/create";
    let codigo = 1;
    let test = false;
    try {
      test = await bd.insertar(params,"USUARIO");
    } finally { 
      if(test){
        res.status(200).send({error_code: codigo, response:{params}, message: nombre_funcion});
      }else{
        codigo = 2;
        res.status(200).send({error_code: codigo, response:{params}, message: nombre_funcion});
      }
    } 
}

/* Condiciones: (Solamente aplica a la tabla principal)
  Sin ninguna condicion: let condiciones = {}
  Con una condicion: let condiciones = {'nombre':'Paquito'}
  Con condiciones AND let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}
  con condiciones OR  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } Ejemplo 2:  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }
  Expresiones siminlares: Ejemplo buscar nombres parecido a Juan: let condiciones = { name: { 'like': '%Juan%' }}
  Negacion: let condiciones = {'nombre': { '!=': 'Paquito' }}; 
 */
async function listarUsuarios(req,res){
  let nombre_funcion = "Usuarios/listarUsuarios"
  let codigo = 2;
  let listado = [];
  let condiciones = {}; 
  let id_tablas = ["id_empresa","id_provincia","id_puesto"];
  try{
    listado = await bd.leerTodos("USUARIO",condiciones,id_tablas)
    if(listado.length > 0){
      codigo = 1;
    }else{
      codigo = 3
    }
  }finally { 
      res.status(200).send({error_code: codigo, response:{listado}, message: nombre_funcion});
  }
}

//Obtenemos los datos de un usuario
/*
  Condiciones: (Solamente aplica a la tabla principal)
  Sin ninguna condicion: let condiciones = {}
  Con una condicion: let condiciones = {'nombre':'Paquito'}
  Con condiciones AND let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}
  con condiciones OR  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } Ejemplo 2:  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }
  Expresiones siminlares: Ejemplo buscar nombres parecido a Juan: let condiciones = { name: { 'like': '%Juan%' }}
  Negacion: let condiciones = {'nombre': { '!=': 'Paquito' }}; 
 */
async function leerUsuario(req,res){
  let nombre_funcion = "Usuarios/leerUsuario"
  let codigo = 2;
  let params = req.query
  let id = 0;
  let listado = [];
  
  if(params){
    id = req.query.q
    let condiciones = {'_id':id}
    let id_tablas = ["id_empresa","id_provincia","id_puesto"];
    try{
      listado = await bd.leerUno("USUARIO",condiciones,id_tablas)
      if (listado.length > 0){
        codigo = 1;
      }else{
        codigo = 3;
      }

    }finally { 
        res.status(200).send({error_code: codigo, response:{listado}, message: nombre_funcion});
    }
  }else{
    codigo = 2
    res.status(200).send({error_code: codigo, response:{listado}, message: nombre_funcion});
  } 
}
/* Funcion para actualizar usuarios: 
  Condiciones: (Solamente aplica a la tabla principal)
  Sin ninguna condicion: let condiciones = {}
  Con una condicion: let condiciones = {'nombre':'Paquito'}
  Con condiciones AND let condiciones = {'nombre':'Paquito','email':'andres@gmail.com'}
  con condiciones OR  let condiciones = { or: [ { 'nombre':'Paquito' },  { age: { '>': 20 } } ] } Ejemplo 2:  let condiciones = { or: [ { 'nombre':'Paquito' },  { 'nombre':'Juan' } ] }
  Expresiones siminlares: Ejemplo buscar nombres parecido a Juan: let condiciones = { name: { 'like': '%Juan%' }}
  Negacion: let condiciones = {'nombre': { '!=': 'Paquito' }}; 
 */
async function actualizarUsuario(req,res){
  let nombre_funcion = "Usuarios/actualizarUsuario"
  let codigo = 3;
  let params = req.body;
  let listado = [];
  if(params){
    let id = req.body._id;
    let condiciones = {'_id':id}
    let test = false;
    try {
      //tipo,condiciones, modeloEsquema
      test = await bd.actualizar("USUARIO",condiciones,params);
    } finally { 
      if(test){
        codigo = 1;
        res.status(200).send({error_code: codigo, response:{params}, message: nombre_funcion});
      }else{
        codigo = 2;
        res.status(200).send({error_code: codigo, response:{params}, message: nombre_funcion});
      }
    } 
  }else{
    res.status(200).send({error_code: codigo, response:{listado}, message: nombre_funcion});
  }
}

async function borrarUsuario(req,res){
  let nombre_funcion = "Usuarios/borrarUsuario"
  let codigo = 2;
  let params = req.query
  let id = 0;
  let test = false; // La variable verifica si se tramito correctamente la solicitud.
  
  if(params){
    id = req.query.q
    let condiciones = {'_id':id}
    try{
      test = await bd.borrar("USUARIO",condiciones)
      if (test){
        codigo = 1;
      }else{
        codigo = 3;
      }

    }finally { 
        res.status(200).send({error_code: codigo, response:{}, message: nombre_funcion});
    }
  }else{
    codigo = 2
    res.status(200).send({error_code: codigo, response:{}, message: nombre_funcion});
  }
}

module.exports = {
    crearUsuario,
    listarUsuarios,
    leerUsuario,
    actualizarUsuario,
    borrarUsuario
};