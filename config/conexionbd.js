var sailsMongoAdapter = require('sails-mongo');
var sailsMysqlAdapter = require('sails-mysql');
require('dotenv').config({path: './config.env'});
// Obtenemos el tipo de base de datos desde el fichero de configuracion.
let tipo = process.env.DB_PROVIDER
let urlDb = process.env.DB_URL;

if(tipo.toUpperCase() == "MONGODB"){
  console.log("Conexion MongoDB");
  var config = {
    adapters: {
      mongodb: sailsMongoAdapter,
    },
    datastores: {
      default: {
        adapter: "mongodb",
        url: urlDb,
        authSource: "admin",
      },
    },
  };
}else if (tipo.toUpperCase() == "MYSQL"){
  console.log("Conexion MySQL");
  var config = {
    adapters: {
      mysql: sailsMysqlAdapter
    },
    datastores: {
      default: {
        adapter: 'mysql',
        url: 'mysql://root@localhost:3306/pruebaORM'
      }
    }
  };
}else{
  console.log("SIN CONFIGURACION DE BASE DE DATOS");
}

module.exports = {
    config
}