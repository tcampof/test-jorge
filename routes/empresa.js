'use strict'

const Express =require('express');
const Controller = require ('../controllers/empresa');

const api = Express.Router();

api.get('/listarEmpresas',Controller.listarEmpresas)

module.exports = api;