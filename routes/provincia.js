'use strict'

const Express =require('express');
const Controller = require ('../controllers/provincia');

const api = Express.Router();

api.get('/listarProvincias',Controller.listarProvincias)

module.exports = api;