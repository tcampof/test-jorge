'use strict'

const Express =require('express');
const Controller = require ('../controllers/usuarios');

const api = Express.Router();

api.post('/crearUsuario',Controller.crearUsuario) // Es llamada por la API de firmafy sin token propio
api.get('/listarUsuarios',Controller.listarUsuarios)
api.get('/leerUsuario',Controller.leerUsuario)
api.put('/actualizarUsuario',Controller.actualizarUsuario)
api.delete('/borrarUsuario',Controller.borrarUsuario)


module.exports = api;