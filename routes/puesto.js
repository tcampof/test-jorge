'use strict'

const Express =require('express');
const Controller = require ('../controllers/puesto');

const api = Express.Router();

api.get('/listarPuestos',Controller.listarPuestos)

module.exports = api;